<?php

/**
 * @file
 * Profile file.
 */

/**
 * Implements hook_form_alter().
 */
function pesquisar_form_alter(&$form, &$form_state, $form_id) {
  // Add placeholder to search box
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['placeholder'] = 'Pesquisarrrrrrr';
  }
}